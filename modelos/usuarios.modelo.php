<?php

  require_once "conexion.php";

  class ModeloUsuarios{

    /*=============================================
    CONUSULTA USUARIOS
    =============================================*/

    static public function mdlConsultaUsuario($tabla, $correo){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE correo = '$correo'");

      $stmt -> execute();

      return $stmt -> fetch();
    
      $stmt = null;

    }
    static public function consultaUsuarios($tabla){
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE tipo_usuario != 3 ORDER BY fecha_registro DESC");

      $stmt -> execute();

      return $stmt -> fetchAll();      

      $stmt = null;

    }
    static public function consultaHangers($tabla){
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE tipo_usuario = 1");

      $stmt -> execute();

      return $stmt -> fetchAll();
      
      $stmt = null;

    }
    static public function consultaNumeroDisenos($tabla, $idUsuario){
      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) AS total FROM $tabla WHERE id_usuario = '$idUsuario' AND diseno_en_venta = 0");

      $stmt -> execute();

      return $stmt -> fetch();
    
      $stmt = null;

    }
    static public function consultaNumeroVotos($tabla, $idUsuario){
      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_voto) AS total FROM $tabla WHERE id_usuario = '$idUsuario'");

      $stmt -> execute();

      return $stmt -> fetch();      

      $stmt = null;

    }


    static public function consultaMedallas($tabla1){

      $stmt = Conexion::conectar()->prepare("SELECT id_insignia, ruta_insignia, nombre_insignia FROM $tabla1 ORDER BY orden ASC");

      $stmt -> execute();

      return $stmt -> fetchAll();
      
      $stmt = null;

    }

    static public function consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(*)  FROM $tabla1  WHERE id_usuario = '$idUsuario' AND id_insignia = '$insigniasId'");

      $stmt -> execute();

      return $stmt -> fetch();
      
      $stmt = null;

    }

    static public function consultaDisenosTienda($tabla1, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_diseno) AS total FROM $tabla1  WHERE id_usuario = '$idUsuario' AND diseno_en_venta=1");

      $stmt -> execute();

      return $stmt -> fetch();      

      $stmt = null;

    }

    static public function consultaDisenoMasVotos($tabla1, $tabla2, $idUsuario){

      $stmt = Conexion::conectar()->prepare("SELECT t1.id_diseno AS id_diseno, t1.nombre_diseno AS nombre_diseno, (SELECT COUNT(id_voto) AS votos FROM $tabla2 WHERE voto_activo = 1 AND id_diseno = t1.id_diseno) AS votos FROM $tabla1 AS t1 WHERE t1.id_usuario = '$idUsuario' ORDER BY votos DESC LIMIT 1");

      $stmt -> execute();

      return $stmt -> fetch();      

      $stmt = null;

    }
    static public function consultaPaisUsuario($tabla, $idPais){

      $stmt = Conexion::conectar()->prepare("SELECT nombre_pais FROM $tabla WHERE id = '$idPais'");

      $stmt -> execute();

      return $stmt -> fetch();  

      $stmt = null;

    }
    static public function consultaEstadoUsuario($tabla, $idEstado){

      $stmt = Conexion::conectar()->prepare("SELECT estado FROM $tabla WHERE sk_estado = '$idEstado'");

      $stmt -> execute();

      return $stmt -> fetch();      

      $stmt = null;

    }

    static public function mdlUpdateUsuario($query){

      // echo $query;

  		$stmt = Conexion::conectar()->prepare($query);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}
  	
  		$stmt = null;

  	}

    static public function mdlDatosUsuario($tabla, $id_usuario){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = '$id_usuario'");

      $stmt -> execute();

      return $stmt -> fetch();      

      $stmt = null;

    }

  }
