<?php

  require_once "conexion.php";

  class ModeloInsignias{
    static public function mdlAgregarInsignia($tabla,$id_insignia_usuario,$id_usuario,$id_insignia){

      $stmt = Conexion::conectar()->prepare("INSERT INTO insignias_usuarios (id_insignia_usuario, id_usuario, id_insignia) VALUES (:id_insignia_usuario,:id_usuario, :id_insignia)");

      $stmt->bindParam(":id_insignia_usuario", $id_insignia_usuario, PDO::PARAM_STR);
  		$stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_STR);
      $stmt->bindParam(":id_insignia", $id_insignia, PDO::PARAM_STR);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;

    }

    static public function mdlEliminarInsignia($tabla,$id_usuario,$id_insignia){      

  		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario AND id_insignia = :id_insignia");

  		$stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_STR);
      $stmt->bindParam(":id_insignia", $id_insignia, PDO::PARAM_STR);


  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;

  	}

    static public function generaUUID(){

      $stmt = Conexion::conectar()->prepare("SELECT UUID() AS uuid");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }


  }
