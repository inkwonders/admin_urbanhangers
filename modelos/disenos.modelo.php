<?php

  require_once "conexion.php";

  class ModeloDisenos{

    static public function consultaDisenos($tabla){

      $stmt = Conexion::conectar()->prepare("SELECT id_diseno,clave_diseno,ruta_img,nombre_diseno,inspiracion,fecha_diseno,
        (SELECT nombre_coleccion FROM colecciones WHERE sk_coleccion = diseno.fk_coleccion LIMIT 0,1) AS nombre_coleccion,
        (SELECT carpeta FROM usuarios WHERE id_usuario = diseno.id_usuario LIMIT 0,1) AS carpeta,
        (SELECT usuario FROM usuarios WHERE id_usuario = diseno.id_usuario LIMIT 0,1) AS usuario,
        id_usuario,tendencia,fecha_tendencia,diseno_en_venta,fecha_tienda,ruta_tienda,ruta,activo FROM diseno ORDER BY fecha_diseno DESC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaVotos($tabla1, $idDiseno){

      $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) AS contador FROM votos_diseno WHERE id_diseno = '$idDiseno' AND voto_activo = 1");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaComentarios($tabla1, $idDiseno){

      $stmt = Conexion::conectar()->prepare("SELECT t1.modo_registro AS modo_registro, t1.nombre AS nombre, t2.comentario AS comentario, t1.foto AS foto, t2.fecha AS fecha, t2.id_diseno AS id_diseno, t1.sexo AS sexo, t1.tipo_usuario AS tipo_usuario, t1.carpeta AS carpeta, t1.usuario AS usuario FROM $tabla1 AS t1, comentarios AS t2 WHERE t1.id_usuario= t2.id_usuario AND t2.id_diseno='$idDiseno'");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function consultaImgDiseno($tabla1, $key){

      $stmt = Conexion::conectar()->prepare("SELECT ruta_img FROM $tabla1 WHERE id_diseno = '$key'");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlUpdateDiseno($query){

      // echo $query;

  		$stmt = Conexion::conectar()->prepare($query);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;

  	}

    static public function mdlConsultaDisenosUsuarios(){

      $stmt = Conexion::conectar()->prepare("SELECT id_diseno, clave_diseno, nombre_diseno, id_usuario, ruta,ruta_img, (SELECT usuario FROM usuarios WHERE id_usuario = diseno.id_usuario LIMIT 0,1) AS usuario,(SELECT carpeta FROM usuarios WHERE id_usuario = diseno.id_usuario LIMIT 0,1) AS carpeta,(SELECT ruta FROM usuarios WHERE id_usuario = diseno.id_usuario LIMIT 0,1) AS ruta_usuario,carpeta_mockup,fecha_diseno FROM diseno ORDER BY fecha_diseno DESC");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlConsultaMockupsDiseno($id_diseno){

      $query = "SELECT (SELECT CONCAT(tipo_producto,' ', descripcion) FROM productos_base WHERE sk_producto = mockup.fk_producto LIMIT 0,1) AS producto, (SELECT genero FROM productos_base WHERE sk_producto = mockup.fk_producto LIMIT 0,1) AS genero ,nombre_archivo, nombre_original FROM mockup WHERE fk_diseno = '$id_diseno' ORDER BY fecha_creacion DESC";

      $stmt = Conexion::conectar()->prepare($query);

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }

    static public function mdlConsultaTotalMockupsDiseno($id_diseno){

      $query = "SELECT count(*) AS total FROM mockup WHERE fk_diseno = '$id_diseno'";

      $stmt = Conexion::conectar()->prepare($query);

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }

  }
