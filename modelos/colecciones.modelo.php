<?php

  require_once "conexion.php";

  class ModeloColecciones{
    static public function consultaColecciones($tabla){
      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fecha_alta");

      $stmt -> execute();

      return $stmt -> fetchAll();

      $stmt -> close();

      $stmt = null;

    }
    static public function mdlUpdateColeccion($query){

      //echo $query;

  		$stmt = Conexion::conectar()->prepare($query);

  		if($stmt->execute()){

  			return "ok";

  		}else{

  			return "error";

  		}

  		$stmt->close();
  		$stmt = null;

  	}

    static public function generaUUID(){

      $stmt = Conexion::conectar()->prepare("SELECT UUID() AS uuid");

      $stmt -> execute();

      return $stmt -> fetch();

      $stmt -> close();

      $stmt = null;

    }



  }
