<div class="encabezado barra_home">
        <div class="cont_encabezado_home">
            <div id="encabezado_vista" class="encabezado-celda">
            </div>
            <div id="encabezado_categoria">
                <div class="titulo">DISEÑOS</div>
            </div>
            <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
            </div>
        </div>
    </div>
    <div class="contenedor_interno">
        <table id="datatable_2" class="display" style="width:100%">
            <thead>
                <tr>
                    <th style="display:none;">Fecha Registro</th>
                    <th class="t_imagen">Imagen</th>
                    <th class="t_nombre">Nombre</th>
                    <th class="t_colecciones">Coleccion</th>
                    <th class="t_tendencia">Tendencia</th>
                    <th class="t_venta">Diseño en venta</th>
                    <th class="t_ruta">Ruta de tienda</th>
                    <th class="t_status_dis">Status</th>
                    <th class="t_acciones_dis">Acciones</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $disenos = ControladorDisenos::consultaDisenos("disenos");

              foreach($disenos as $key => $valueDisenos){
                $id_diseno = $valueDisenos['id_diseno'];
                $id_usuario = $valueDisenos['id_usuario'];
                $clave_diseno = $valueDisenos['clave_diseno'];
                $nombre_diseno = $valueDisenos['nombre_diseno'];
                $inspiracion = $valueDisenos['inspiracion'];
                $fecha_diseno = $valueDisenos['fecha_diseno'];
                $nombre_coleccion = $valueDisenos['nombre_coleccion'];
                $tendencia = $valueDisenos['tendencia'];
                $fecha_tendencia = ( !empty($valueDisenos['fecha_tendencia']) ) ? $valueDisenos['fecha_tendencia'] : '--';
                $tienda = $valueDisenos['diseno_en_venta'];
                $fecha_tienda = ( !empty($valueDisenos['fecha_tienda']) ) ? $valueDisenos['fecha_tienda']  : '--';
                $fecha_tienda = ( !empty($valueDisenos['fecha_tienda']) ) ? $valueDisenos['fecha_tienda']  : '--';
                $ruta_tienda = ( !empty($valueDisenos['ruta_tienda']) ) ? $valueDisenos['ruta_tienda']  : '--';
                $activo = $valueDisenos['activo'];
                $ruta_diseno = $valueDisenos['ruta'];
                ?>
                <tr>
                    <td style="display:none;"><?php echo $fecha_diseno; ?></td>
                    <td class="t_imagen"><img class="imgdista" src="<?php echo $ruta_hangers; ?>vistas/assets/hangers/<?php echo $valueDisenos['carpeta']; ?>/<?php echo $valueDisenos['ruta_img']; ?>" alt="<?php echo $nombre_diseno; ?>" title="<?php echo $nombre_diseno; ?>"></td>
                    <td class="t_nombre"><a class="link_disenos" href="<?php echo $ruta_hangers.$ruta_diseno; ?>" target="_blank"><?php echo $nombre_diseno; ?></a></td>
                    <td class="t_colecciones"><?php echo $nombre_coleccion; ?></td>
                    <td class="t_tendencia">
                      <?php
                        if($tendencia == 1){
                          ?>
                          <a id="tendencia_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Quitar tendencia" val="<?php echo $tendencia; ?>" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light green evento_tendencia"><i class="material-icons">trending_up</i></a>
                          <?php
                        }else {
                          ?>
                          <a id="tendencia_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Volver tendencia" val="<?php echo $tendencia; ?>" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light red evento_tendencia"><i class="material-icons">trending_down</i></a>
                          <?php
                        }
                        ?>
                    </td>
                    <td id="td_tienda_<?php echo $clave_diseno; ?>" class="t_venta">
                      <?php
                        if($tienda == 1){
                          echo "Sí";
                        }else {
                          ?>
                          <a id="tienda_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Pasar diseño a tienda" val="<?php echo $tienda; ?>" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light deep-orange darken-1 evento_tienda"><i class="material-icons">add_shopping_cart</i></a>
                          <?php
                        }
                        ?>
                    </td>
                    <td id="td_ruta_<?php echo $clave_diseno; ?>" class="t_ruta">
                      <?php
                      if($ruta_tienda == '--'){
                         echo $ruta_tienda;
                      }else {
                        ?>
                        <span id="ruta_tienda_<?php echo $clave_diseno; ?>" class="evento_inp_ruta ruta_diseno" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" title='<?php echo $ruta_tienda; ?>'><?php echo $ruta_tienda; ?></span>
                        <?php
                      }
                      ?>
                    </td>
                    <td class="t_status_dis">
                      <?php
                        if($activo == 1){
                          ?>
                          <a id="estatus_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Inactivar diseño" val="<?php echo $activo; ?>" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light green evento_estatus"><i class="material-icons">done</i></a>
                          <?php
                        }else {
                          ?>
                          <a id="estatus_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Activar diseño" val="<?php echo $activo; ?>" no="<?php echo $clave_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light red evento_estatus"><i class="material-icons">do_not_disturb_alt</i></a>
                          <?php
                        }
                      ?>
                    </td>
                    <td id="td_acciones_<?php echo $clave_diseno; ?>" class="t_acciones_dis">
                      <a id="info_<?php echo $clave_diseno; ?>" data-position="bottom" data-tooltip="Ver más información" nombre="<?php echo $nombre_diseno; ?>" insp ="<?php echo $inspiracion; ?>" cd="<?php echo base64_encode($valueDisenos['carpeta']); ?>" key="<?php echo base64_encode($id_diseno); ?>" key2="<?php echo base64_encode($id_usuario); ?>" class="btn_info_dis tooltipped btn-floating btn-medium waves-effect waves-light grey darken-4 evento_info"><i class="material-icons">search</i></a>
                      <?php
                      if($ruta_tienda != '--'){
                        ?>
                        <a id="go_<?php echo $clave_diseno; ?>" href="<?php echo $ruta_tienda; ?>" target="_blank" data-position="bottom" data-tooltip="Ver en tienda" class="tooltipped btn-floating btn-medium waves-effect waves-light blue darken-1"><i class="material-icons">shopping_bag</i></a>
                        <?php
                      }
                      ?>
                    </td>
                </tr>
                <?php
              }
              ?>
            </tbody>
        </table>
    </div>

    <div id="modal_detalle" class="modal modal_dashboard_diseno">
      <span class="close_modal_detalle">X</span>
      <div id="datos_modal_dashboard" class="modal-content wh_100 fx_modal_content_dashboard" style="padding:0"></div>
    </div>



    <script type="text/javascript">

    $(".evento_info").click(function() {
      let key = $(this).attr('key');
      let key_user = $(this).attr('key2');
      let nombre_diseno = $(this).attr('nombre');
      let inspiracion_diseno = $(this).attr('insp');
      let carpeta = $(this).attr('cd');
      $('#modal_detalle').modal('open');
      datosModalDiseno(key, carpeta, nombre_diseno, inspiracion_diseno,key_user);
    });

    $(document).on('click','.evento_inp_ruta',function() {
      let key_user = $(this).attr('key2');
      let key = $(this).attr('key');
      let no = $(this).attr('no');
      swal({
          title: "Cambiar ruta de tienda",
          text: "¿Estás seguro de querer cambiar la ruta?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Escribe la nueva URL del diseño en tienda:", {
            content: "input",
          })
          .then((value) => {
            // console.log(value);
            if(value != '' && value != null){
              let response = updateDiseno(key_user,key,no,'',value,'ruta_tienda');
              // console.log(response);
              if(response == 'ok'){
                $("#go_"+no).attr('href',value);
                $(this).attr('title',value);
                $(this).html(value);
                swal("El cambio se ha realizado correctamente.", {
                  icon: "success",
                });
              }else {
                swal("Ocurrió un error.", {
                  icon: "error",
                });
              }
            }
          });
        }

      });
    });

    $(".evento_tienda").click(function() {
      let key_user = $(this).attr('key2');
      let key = $(this).attr('key');
      let no = $(this).attr('no');
      let val = $(this).attr('val');

      swal({
          title: "Cambiar a tienda",
          text: "¿Estás seguro de querer pasar el diseño a tienda?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
           swal("URL del diseño en tienda: \n Nota: recuerda colocar http:// ó https:// al inicio", {
            content: "input",
          })
          .then((value) => {
            // console.log(value);
            if(value != '' && value != null){
              let response = updateDiseno(key_user,key,no,val,value,'tienda');
              // console.log(response);
              if(response == 'ok'){
                $("#td_tienda_"+no).html('Sí');
                $("#td_ruta_"+no).html('<span id="ruta_tienda_'+no+'" class="evento_inp_ruta ruta_diseno" no="'+no+'" key="'+key+'" key2="'+key_user+'" title="'+value+'">'+value+'</span>');
                $("#td_acciones_"+no).append('<a id="go_'+no+'" href="'+value+'" target="_blank" data-position="bottom" data-tooltip="Ver en tienda" class="tooltipped btn-floating btn-medium waves-effect waves-light blue darken-1"><i class="material-icons">shopping_bag</i></a>');
                swal("El cambio se ha realizado correctamente.", {
                  icon: "success",
                });
              }else {
                swal("Ocurrió un error.", {
                  icon: "error",
                });
              }
            }
          });
        }

      });

    });

    $(".evento_estatus").click(function() {
      let key_user = $(this).attr('key2');
      let key = $(this).attr('key');
      let no = $(this).attr('no');
      let val = $(this).attr('val');
      swal({
          title: "Cambiar status",
          text: "¿Estás seguro de querer cambiar el status del diseño?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
          let response = updateDiseno(key_user,key,no,val,'','activo');
          // console.log(response);
          if(response == 'ok'){
            actualiza_botones(this,'boton',val,'do_not_disturb_alt','done','Activar diseño','Inactivar diseño')
            swal("El cambio se ha realizado correctamente.", {
              icon: "success",
            });
          }else {
            swal("Ocurrió un error.", {
              icon: "error",
            });
          }
        }
      });

    });

    $(".evento_tendencia").click(function() {
      let key = $(this).attr('key');
      let no = $(this).attr('no');
      let val = $(this).attr('val');
      let key_user = $(this).attr('key2');

      swal({
          title: "Cambiar tendencia",
          text: "¿Estás seguro de querer cambiar tendencia del diseño?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
          let response = updateDiseno(key_user,key,no,val,'','tendencia');
          // console.log(response);
          if(response == 'ok'){
            actualiza_botones(this,'boton',val,'trending_down','trending_up','Volver tendencia','Quitar tendencia')
            swal("El cambio se ha realizado correctamente.", {
              icon: "success",
            });
          }else {
            swal("Ocurrió un error.", {
              icon: "error",
            });
          }
        }
      });

    });

    $(".close_modal_detalle").click(function (){
      $('#modal_detalle').modal("close");
    });


    function datosModalDiseno(key, carpeta, nombre_diseno, inspiracion_diseno,id_usuario_modal){

      let datos = {
        "id_diseno": key,
        "carpeta": carpeta,
        "nombre_diseno" : nombre_diseno,
        "inspiracion_diseno" : inspiracion_diseno,
        "id_usuario": id_usuario_modal
      }

      $.ajax({
      	url: 'ajax/modalDiseno.ajax.php',
        data: datos,
        type: "POST",
        beforeSend: function(){
          $('#datos_modal_dashboard').html(loader());
        },
      	success: function(respuesta) {
          // console.log(respuesta);
          $('#datos_modal_dashboard').html(respuesta);
      	}
      });

    }


    function updateDiseno(key_user,key,clave_diseno,status,ruta_tienda,tipo){

      let res = '';
      let datos = {
        "key_user" : key_user,
        "key": key,
        "no": clave_diseno,
        "status" : status,
        "ruta_tienda" : ruta_tienda,
        "tipo": tipo
      }

      $.ajax({
      	url: 'ajax/updateDisenos.ajax.php',
        data: datos,
        type: "POST",
        async : false,
        beforeSend : function(){
          console.log(datos);
        },
      	success: function(respuesta) {
          res = respuesta;
      	}
      });
      return res;
    }

    function actualiza_botones(elemento,tipo,valor,icon1,icon2,txt1,txt2){
      if(tipo == 'ruta'){

      }else {
        if(valor == 1){
          $(elemento).attr('val','0');
          $(elemento).attr('data-tooltip',txt1);
          $(elemento).removeClass('green');
          $(elemento).addClass('red');
          $(elemento).html('<i class="material-icons">'+icon1+'</i>');
        }else {
          $(elemento).attr('val','1');
          $(elemento).attr('data-tooltip',txt2);
          $(elemento).removeClass('red');
          $(elemento).addClass('green');
          $(elemento).html('<i class="material-icons">'+icon2+'</i>');
        }
      }
    }

    </script>
