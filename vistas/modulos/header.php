<header>
  <div class="cont_header">
      <img class="logo-head" src="<?php echo $ruta_hangers; ?>vistas/assets/img/logo.svg" id="logo-ss">
      <div class="menu-contender">
          <ul class="menu-nav" id="menu-slide">
              <a class="no_link" href="<?php $ruta_global ?>usuarios"><li class="menu-el" id="bUsuarios">Usuarios</li></a>
              <a class="no_link" href="<?php $ruta_global ?>disenos"><li class="menu-el" id="bDisenos">Diseños</li></a>
              <a class="no_link" href="<?php $ruta_global ?>mockups"><li class="menu-el" id="bMockups">Mockups</li></a>
              <a class="no_link" href="<?php $ruta_global ?>colecciones"><li class="menu-el" id="bColecciones">Colecciones</li></a>
              <a class="no_link" href="<?php $ruta_global ?>insignias"><li class="menu-el" id="bInsignias">Insignias</li></a>
              <a href="salir"><li class="menu-el border-hover hov-usuario login_usuario" style="text-align:center">
                  <div class="icon_menu_log icon-usuario" title="Usuario" style="display:inline-block"></div><br>
                  <span class="usuario_login" id="nombreLog">Cerrar Sesión</span>
              </li></a>
          </ul>
      </div>
  </div>
</header>
<?php
// unset($_SESSION["iniciarSesion"]);
?>
