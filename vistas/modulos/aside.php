<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="home" class="brand-link imagen_logo">
        <img class="imagen_logo" src="<?php echo $ruta_global; ?>vistas/assets/img/logo.svg">
    </a>

    <div class="sidebar">

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <div class="nav-link menu_admin" op="panel">
                                <i class="nav-icon fas fa-th"></i>
                                <p>Panel</p>
                            </div>
                        </li>
                    </ul>

                </li>
            </ul>
        </nav>

    </div>

</aside>
