<div class="encabezado barra_home">
        <div class="cont_encabezado_home">
            <div id="encabezado_vista" class="encabezado-celda">
            </div>
            <div id="encabezado_categoria">
                <div class="titulo">COLECCIONES</div>
            </div>
            <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
            </div>
        </div>
    </div>


    <div class="contenedor_interno">
      <div id="agregar_coleccion">
        <a data-tooltip="Agregar coleccion" class="tooltipped btn-floating btn-medium waves-effect waves-light evento_agregar_coleccion red"><i class="material-icons">add_circle</i></a>
      </div>
        <table id="datatable_3" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Status</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $peticion = Colecciones::consultaColecciones("colecciones");

              foreach($peticion as $key => $colecciones){
                ?>
                <tr>
                    <td><?php echo $colecciones["nombre_coleccion"]; ?></td>
                    <?php if ($colecciones["activo"] == 1) {?>
                      <td><a id="estatus_<?php echo base64_encode($colecciones["sk_coleccion"]);?>" val="1" data-position="bottom" data-tooltip="Desactivar coleccion" key_coleccion="<?php echo base64_encode($colecciones["sk_coleccion"]);?>" status="<?php echo $colecciones["activo"];?>" class="tooltipped btn-floating btn-medium waves-effect waves-light green evento_estatus_coleccion"><i class="material-icons">done</i></a></td>
                    <?php }else{ ?>
                      <td><a id="estatus_<?php echo base64_encode($colecciones["sk_coleccion"]);?>" val="0" data-position="bottom" data-tooltip="Activar coleccion" key_coleccion="<?php echo base64_encode($colecciones["sk_coleccion"]);?>" status="<?php echo $colecciones["activo"];?>" class="tooltipped btn-floating btn-medium waves-effect waves-light evento_estatus_coleccion red"><i class="material-icons">do_not_disturb_alt</i></a></td>
                      <?php } ?>

                    <td><?php echo $colecciones["fecha_alta"]; ?></td>

                </tr>
                <?php
              }
                ?>
            </tbody>
            <!-- <tfoot>
                <tr>
                    <th>Nombre</th>
                    <th>Posición</th>
                    <th>Oficina</th>
                    <th>Edad</th>
                    <th>Fecha de inicio</th>
                    <th>Sueldo</th>
                </tr>
            </tfoot> -->
        </table>
    </div>

    <script>

    $(".evento_agregar_coleccion").click(function() {

      swal({
          title: "Agregar nueva coleccion",
          content: "input"
      })
      .then((value) => {
            console.log(value);
            if(value != '' && value != null){
              let response = updateColeccion("","",'agregar_coleccion',value);
              if(response == 'ok'){
                swal("Se ha agregado la colección con éxito.", {
                  icon: "success",
                });
              }else {
                swal("Ocurrió un error.", {
                  icon: "error",
                });
              }
            }else{
              swal("Por favor agrega un nombre de colección.", {
                icon: "error",
              });
            }
          });
    });

    $(".evento_estatus_coleccion").click(function(){
      let key_coleccion = $(this).attr('key_coleccion');
      let status = $(this).attr('status');
      let val = $(this).attr('val');
      swal({
          title: "Cambiar status",
          text: "¿Estás seguro de querer cambiar el status de la coleccion?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
          let response = updateColeccion(key_coleccion,val,'activo');
          if(response == 'ok'){
            actualiza_botones(this,'boton',val,'do_not_disturb_alt','done','Activar usuario','Inactivar usuario')
            swal("El cambio se ha realizado correctamente.", {
              icon: "success",
            });
          }else {
            swal("Ocurrió un error.", {
              icon: "error",
            });
          }
        }
      });

    });

    function updateColeccion(key_coleccion,val,tipo,value){

      let res = '';
      let datos = {
        "key_coleccion" : key_coleccion,
        "status" : val,
        "tipo": tipo,
        "value": value
      }

      $.ajax({
        url: 'ajax/updateColeccion.ajax.php',
        data: datos,
        type: "POST",
        async : false,
        success: function(respuesta) {
          res = respuesta;
          if(tipo =="agregar_coleccion"){
          setTimeout(function(){
           location.reload();
         }, 1500);}
       }
      });
      return res;
    }

    function actualiza_botones(elemento,tipo,valor,icon1,icon2,txt1,txt2){
      if(tipo == 'ruta'){

      }else {
        if(valor == 1){
          $(elemento).attr('val','0');
          $(elemento).attr('data-tooltip',txt1);
          $(elemento).removeClass('green');
          $(elemento).addClass('red');
          $(elemento).html('<i class="material-icons">'+icon1+'</i>');
        }else {
          $(elemento).attr('val','1');
          $(elemento).attr('data-tooltip',txt2);
          $(elemento).removeClass('red');
          $(elemento).addClass('green');
          $(elemento).html('<i class="material-icons">'+icon2+'</i>');
        }
      }
    }
    </script>
