<div class="login-box">


    <div class="card">
        <div class="card-body login-card-body">
            <div class="login-logo">
                <a href="">
                    <img src="<?php echo $ruta_hangers; ?>vistas/assets/img/logo.svg">
                </a>
            </div>
            <br>

            <form id="form_ingreso" method="post">
                <div class="input-field input-group mb-3">
                    <label class="label-inicio" for="email" id="for_email">CORREO</label>
                    <input type="email" class="input-inicio" name="email_login" id="email" required>
                    <!-- <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-envelope"></i>
                        </div>
                    </div> -->
                </div>
                <div class="input-field input-group mb-3">
                    <label class="label-inicio" for="email">CONTRASEÑA</label>
                    <input type="password" class="input-inicio" name="contraseña_login" id="password" required>
                    <img src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-mostrar.svg" class="mostrar-pass mostrar_password" inp="password" id="mostrar_pass_login" title="Mostrar contraseña">
                    <!-- <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-key"></i>
                        </div>
                    </div> -->
                </div>
                <!-- <div class="div-inicio-modal">
                    <p>
                        <label>
                            <input id="check_recordar" type="checkbox" name="recordar">
                            <span>Recuérdame</span>
                        </label>
                    </p>
                    <a class="enlace-pass" onclick="reestablecer_contrasena()">¿Olvidaste tu contraseña?</a>
                </div> -->
                <br>
                <br>
                <div id="cont_botones_inicio">
                    <input type="submit" class="button-inicio" value="INICIAR SESIÓN"> <br>
                </div>
                <div class="respuesta_clave"></div>
                <?php

                    $ingreso = new Usuarios();
                    $ingreso->ctrIngreso();

                ?>
                <!-- <div class="row btn_aceptar">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Ingresar</button>



                    </div>
                </div> -->
            </form>

        </div>
    </div>

</div>
<script type="text/javascript">
    $("#mostrar_pass_login").click(function() {
        let input = $(this).attr("inp");
        let tipo = $("#" + input).attr("type");
        // console.log("tipo "+tipo+" input "+input);
        // console.log($(this).attr("inp"));
        if (tipo == 'password') {
            $('#' + input).attr('type', 'text');
            $(this).attr('src', '<?php echo $ruta_hangers; ?>vistas/assets/img/icon-ocultar.svg');
            $(this).attr('title', 'Ocultar contraseña');
        } else {
            $('#' + input).attr('type', 'password');
            $(this).attr('src', '<?php echo $ruta_hangers; ?>vistas/assets/img/icon-mostrar.svg');
            $(this).attr('title', 'Mostrar contraseña');
        }
    });
    /*$("#form_ingreso").submit(function(e) {
        e.preventDefault();
        let datos = $(this).serialize();
        $.ajax({
            url: "ajax/ingreso.ajax.php",
            type: "POST",
            data: datos,
            beforeSend: function(){
                $("#cont_botones_inicio").addClass("deshabilitado");
                $("#footer_modal_inicio").addClass("deshabilitado");
                $("#div_respuesta_login").html("<img src='vistas/assets/css/ajax-loader.gif' alt='Cargando...'>");
            },
            success: function(response) {
                $("#cont_botones_inicio").removeClass("deshabilitado");
                $("#footer_modal_inicio").removeClass("deshabilitado");
                $("#cont_botones_inicio").addClass("habilitado");
                $("#footer_modal_inicio").addClass("habilitado");
                if (response == "ok") {
                    location.reload();
                } else {
                    if (response == 'usuario_inactivo') {
                        $("#div_respuesta_login").html("<p class='error_fb'>Usuario inactivo.</p>");
                    } else if (response == "datos") {
                        $("#div_respuesta_login").html("<p class='error_fb'>Datos incorrectos, favor de revisarlos.</p>");
                    } else if (response == "verificacion") {
                        $("#div_respuesta_login").html("<p class='error_fb'>Favor de verificar cuenta.</p>");
                    } else if (response == "modo") {
                        $("#div_respuesta_login").html("<p class='error_fb'>Modo de registro incorrecto.</p>");
                    } else {
                        $("#div_respuesta_login").html(response);
                    }
                }
            }
        });
    });*/
</script>
