<div class="encabezado barra_home">
        <div class="cont_encabezado_home">
            <div id="encabezado_vista" class="encabezado-celda">
            </div>
            <div id="encabezado_categoria">
                <div class="titulo">USUARIOS</div>
            </div>
            <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
            </div>
        </div>
    </div>
    <div class="contenedor_interno">
      <div id="exp_usuarios">
        <div id="icon-hanger">
          <img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-hanger.svg"> <span> :hanger</span>
        </div>
        <div id="icon-votante">
        <img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-votante.svg"> <span> :votante</span>
        </div>
      </div>
        <table id="datatable_1" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Avatar</th>
                    <th>Usuario</th>
                    <th>Email</th>
                    <th>Fecha registro</th>
                    <th>Género</th>
                    <th>Diseños en tienda</th>
                    <th>Diseños en votacion</th>
                    <th>Diseño con más votos</th>
                    <th>Total votos</th>
                    <th>Status</th>
                    <th>Ver más</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $peticion = Usuarios::consultaUsuarios("usuarios");

              foreach($peticion as $key => $usuarios){
                $idUsuario = $usuarios["id_usuario"];
                ?>
                <tr>


                  <!---tipo usuario-->
                  <?php if($usuarios["tipo_usuario"] == 1){?>
                      <td><img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-hanger.svg"></td>
                    <?php }else{ ?>
                      <td><img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-votante.svg"></td>
                    <?php } ?>


                    <!--foto-->
                    <?php
                    $foto_bandera = 1;
                    if($usuarios["modo_registro"] =="directo" && $usuarios["tipo_usuario"]==1){
                      if(!empty($usuarios["foto"])){
                        $ruta_foto_modal = $ruta_hangers.'vistas/assets/hangers/'.$usuarios["carpeta"].'/'.$usuarios["foto"];
                        $foto_modal = "<img src='".$ruta_foto_modal."'>";
                        ?>
                        <td><img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/hangers/<?php echo $usuarios["carpeta"];?>/<?php echo $usuarios["foto"];?>"></td>
                        <?php
                      }else{
                        $foto_modal = '<div class="dv_sin_foto_modal"><span class="span_sin_foto">Sin foto</span></div>';
                        $foto_bandera = 0;
                        ?>
                        <td>SIN FOTO</td>
                        <?php
                      }
                    }else if($usuarios["modo_registro"] =="facebook" && $usuarios["tipo_usuario"]==1){
                      $ruta_foto_modal = $usuarios["foto"];
                      $foto_modal = "<img src='".$ruta_foto_modal."'>";
                      ?>
                      <td><img class="img_iconos" src="<?php echo $usuarios["foto"];?>"></td>
                      <?php
                    }else if($usuarios["modo_registro"] =="facebook" && $usuarios["tipo_usuario"]==2){
                      $ruta_foto_modal = $usuarios["foto"];
                      $foto_modal = "<img src='".$ruta_foto_modal."'>";
                      ?>
                      <td><img class="img_iconos" src="<?php echo $usuarios["foto"];?>"></td>
                      <?php
                    }else if($usuarios["modo_registro"] =="directo" && $usuarios["tipo_usuario"]==2){
                      if($usuarios["sexo"]=="h"){
                        $ruta_foto_modal = $ruta_hangers.'vistas/assets/img/icon-usuario-1.svg';
                        $foto_modal = "<img src='".$ruta_foto_modal."'>";
                        ?>
                        <td><img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-usuario-1.svg"></td>
                        <?php
                      }else{
                        $ruta_foto_modal = $ruta_hangers.'vistas/assets/img/icon-usuario-5.svg';
                        $foto_modal = "<img src='".$ruta_foto_modal."'>";
                        ?>
                        <td><img class="img_iconos" src="<?php echo $ruta_hangers; ?>vistas/assets/img/icon-usuario-5.svg"></td>
                        <?php
                      }
                    }
                    ?>
                  <!--fin de foto-->


                  <!--usuario-->
                  <?php
                  if($usuarios["tipo_usuario"]==1){
                  ?>
                    <td><a class="link_disenos" href="<?php echo $ruta_hangers.$usuarios["ruta"];?>" target="_blank"><?php echo $usuarios["usuario"];?></a></td>
                    <?php
                  }else{
                    ?>
                    <td><?php echo $usuarios["usuario"];?></td>
                    <?php
                  }
                    ?>

                    <td><?php echo $usuarios["correo"];?></td>

                    <!-- fecha registro -->
                    <td><span style='display: none;'><?php echo strtotime($usuarios["fecha_registro"]); ?></span><?php echo date("d/m/Y G:ia",strtotime($usuarios["fecha_registro"]));?></td>

                    <!--género-->
                    <?php
                    if($usuarios["sexo"]=="h"){
                    ?>
                    <td>Hombre</td>
                    <?php
                  }else{
                    ?>
                    <td>Mujer</td>
                    <?php
                  }
                    ?>


                    <!---Número de diseños en tienda -->
                    <?php
                    $no_disenos_venta = Usuarios::consultaDisenosTienda("diseno", $idUsuario);
                    ?>
                    <td><?php echo $no_disenos_venta['total']; ?></td>

                    <!---Número de diseños en votacion -->
                    <?php
                    $no_disenos = Usuarios::consultaNumeroDisenos("diseno", $idUsuario);
                    ?>
                    <td><?php echo $no_disenos['total']; ?></td>



                    <!---Diseño con más votos-->
                    <?php
                    $diseno_mas_votado = Usuarios::consultaDisenoMasVotos("diseno", "votos_diseno", $idUsuario);
                    if(!empty($diseno_mas_votado['nombre_diseno'])){
                    ?>
                      <td><?php echo $diseno_mas_votado['nombre_diseno']; ?></td>
                    <?php }else{ ?>
                      <td>Sin votos</td>
                    <?php } ?>

                    <!---Número de votos-->
                    <?php
                    $no_votos = Usuarios::consultaNumeroVotos("votos_diseno", $idUsuario);
                    if($no_votos['total'] != 0){
                    ?>
                      <td><?php echo $no_votos['total']; ?></td>
                    <?php }else{ ?>
                      <td>Sin votos</td>
                    <?php } ?>

                    <!--Status usuario-->
                    <?php
                    if($usuarios["activo"]==1){
                    ?>
                    <td><a id="estatus_<?php echo base64_encode($idUsuario)?>" val="1" data-position="bottom" data-tooltip="Desactivar usuario" key_user="<?php echo base64_encode($idUsuario)?>" status="<?php echo base64_encode($usuarios["activo"])?>" class="tooltipped btn-floating btn-medium waves-effect waves-light green evento_estatus_usuario"><i class="material-icons">done</i></a></td>
                    <?php
                  }else{
                    ?>
                    <td><a id="estatus_<?php echo base64_encode($idUsuario)?>" val="0" data-position="bottom" data-tooltip="Activar usuario" key_user="<?php echo base64_encode($idUsuario)?>" status="<?php echo base64_encode($usuarios["activo"])?>" class="tooltipped btn-floating btn-medium waves-effect waves-light evento_estatus_usuario red"><i class="material-icons">do_not_disturb_alt</i></a></td>
                    <?php
                  }
                  $idPais = $usuarios["pais"];
                  $consulta_pais =  Usuarios::consultaPaisUsuario("paises", $idPais);
                  $pais = $consulta_pais[0];
                  if($pais == "México"){
                    $idEstado = $usuarios["estado"];
                    $consulta_estado = Usuarios::consultaEstadoUsuario("estados_bd9aefd2", $idEstado);
                    $estado = $consulta_estado[0];
                  }else{
                    $estado = $usuarios["estado_extranjero"];
                  }


                  ?>
                  <td>
                  <?php if($usuarios["tipo_usuario"] == 1){?>
                  <a id="ver_mas_usuario" nombre_usuario="<?php echo $usuarios["usuario"];?>" nombre="<?php echo $usuarios["nombre"];?>" apellido="<?php echo $usuarios["apellido"];?>" pais="<?php echo $pais;?>" estado="<?php echo $estado;?>" descripcion="<?php echo $usuarios["descripcion"]; ?>" foto="<?php echo base64_encode($foto_modal);?>" foto_bandera="<?php echo $foto_bandera; ?>" genero="<?php echo $usuarios["sexo"];?>" carpeta="<?php echo $usuarios["carpeta"] ?>" tipo_registro="<?php echo $usuarios["modo_registro"]?>" behance="<?php echo $usuarios["behance"];?>" dribble="<?php echo $usuarios["dribble"];?>" instagram="<?php echo $usuarios["instagram"];?>"  class="btn-floating btn-medium waves-effect waves-light grey darken-4 evento_info"><i class="material-icons">search</i></a>
                  <?php } ?>
                  </td>
                </tr>
                <?php
              }
                ?>
            </tbody>

        </table>

    </div>


    <div id="modal_usuarios" class="modal modal_dashboard_diseno">
      <span class="close_modal_detalle">X</span>
      <div id="datos_modal_usuarios" class="modal-content wh_100 fx_modal_content_dashboard" style="padding:0">
        <div id="primer_div">
          <div id="imagen_usuario">
            <div id="cont_imagen_usuario" class="tam_foto_perfil">
            </div>
          </div>
          <div id="nombre_completo">
            <span class="nombre"></span>
            <span class="apellido"></span>
          </div>
          <div id="genero">
            <span class="genero"></span>
          </div>
          <div id="ubicacion">
            <div class="pin"></div>
            <div class="estado_pais">
            <span class="pais"></span><span class="estado"></span><br />
          </div>
          </div>
        </div>


        <div id="datos_usuario">
            <div id="nombre_usuario_modal">
              <p class="nombre_usuario"></p>
            </div>
            <div id="biografia">
              <span class="descripcion"></span>
            </div>
            <div id="redes_sociales">
              <div id="behance"><img src="<?php echo $ruta_hangers; ?>vistas/assets/img/icono-be.svg" class="icono-red"><span class="behance"></span></div>
              <div id="dribble"><img src="<?php echo $ruta_hangers; ?>vistas/assets/img/icono-internet.svg" class="icono-red"><span class="dribble"></span></div>
              <div id="instagram"><img src="<?php echo $ruta_hangers; ?>vistas/assets/img/icono-instagram.svg" class="icono-red"><span class="instagram"></span></div>
            </div>
      </div>
      </div>
    </div>

    <script>

    $(".close_modal_detalle").click(function(){
      $('#modal_usuarios').modal("close");
    });

    $(".evento_estatus_usuario").click(function() {
      let key_user = $(this).attr('key_user');
      let status = $(this).attr('status');
      let val = $(this).attr('val');
      swal({
          title: "Cambiar status",
          text: "¿Estás seguro de querer cambiar el status del usuario?",
          icon: "warning",
          buttons: true,
          dangerMode: false,
          buttons: ["No", "Sí"],
      })
      .then((willDelete) => {
        if (willDelete) {
          let response = updateUsuario(key_user,val,'activo');
          if(response == 'ok'){
            actualiza_botones(this,'boton',val,'do_not_disturb_alt','done','Activar usuario','Inactivar usuario')
            swal("El cambio se ha realizado correctamente.", {
              icon: "success",
            });
          }else {
            swal("Ocurrió un error.", {
              icon: "error",
            });
          }
        }
      });

    });

    function updateUsuario(key_user,val,tipo){

      let res = '';
      let datos = {
        "key_user" : key_user,
        "status" : val,
        "tipo": tipo
      }

      $.ajax({
        url: 'ajax/updateUsuarios.ajax.php',
        data: datos,
        type: "POST",
        async : false,
        success: function(respuesta) {
          console.log(respuesta);
          res = respuesta;
        }
      });
      return res;
    }

    function actualiza_botones(elemento,tipo,valor,icon1,icon2,txt1,txt2){
      if(tipo == 'ruta'){

      }else {
        if(valor == 1){
          $(elemento).attr('val','0');
          $(elemento).attr('data-tooltip',txt1);
          $(elemento).removeClass('green');
          $(elemento).addClass('red');
          $(elemento).html('<i class="material-icons">'+icon1+'</i>');
        }else {
          $(elemento).attr('val','1');
          $(elemento).attr('data-tooltip',txt2);
          $(elemento).removeClass('red');
          $(elemento).addClass('green');
          $(elemento).html('<i class="material-icons">'+icon2+'</i>');
        }
      }
    }

    $(document).on("click", "#ver_mas_usuario",function(){
      let nombre_usuario = $(this).attr("nombre_usuario");
      let pais = $(this).attr("pais");
      let estado = $(this).attr("estado");
      let descripcion = $(this).attr("descripcion");
      let foto = $(this).attr("foto");
      let carpeta = $(this).attr("carpeta");
      let modo_registro = $(this).attr("tipo_registro");
      let bandera_foto = $(this).attr("foto_bandera");
      // if(modo_registro == "directo"){
      //   imagen_modal = "<img src='<?php echo $ruta_hangers; ?>vistas/assets/hangers/"+carpeta+"/"+foto+"'>";
      // }else{
      //   imagen_modal = "<img src='"+foto+"'>";
      // }

      if(bandera_foto == 0){
        $("#imagen_usuario").addClass('cont_img_modal_fx');
      }else {
        $("#imagen_usuario").removeClass('cont_img_modal_fx');
      }
      let imagen_modal = atob(foto);
      let genero = $(this).attr("genero");
      let nombre = $(this).attr("nombre");
      let apellido = $(this).attr("apellido");
      let behance = $(this).attr("behance");
      let dribble = $(this).attr("dribble");
      let instagram = $(this).attr("instagram");

      $(".nombre_usuario").html(nombre_usuario);
      if(pais != "" || estado != ""){
        $(".pin").html("<img src='<?php echo $ruta_hangers; ?>vistas/assets/img/icono-pos.svg' class='icono-pos'>");
        $(".pais").html(pais);
        if(pais != "" && estado != ""){
          $(".estado").html(", "+estado);
        }else if(pais == "" && estado != ""){
          $(".estado").html(estado);
        }
      }
      $(".descripcion").html(descripcion);
      $("#cont_imagen_usuario").html(imagen_modal);
      if (genero == "h"){
      $(".genero").html("Hombre");
      }else{
      $(".genero").html("Mujer");
      }
      $(".nombre").html(nombre);
      $(".apellido").html(apellido);
      if(behance != ""){
      $(".behance").html("<a class='link_disenos' href='https://www.behance.net/"+behance+"' target='_blank'>"+behance+"</a>");
      }else{
      $(".behance").html("Sin definir");
      }

      if(dribble != ""){
      $(".dribble").html("<a class='link_disenos' href='https://www.dribbble.com/"+dribble+"' target='_blank'>"+dribble+"</a>");
      }else{
      $(".dribble").html("Sin definir");
      }

      if(instagram != ""){
      $(".instagram").html("<a class='link_disenos' href='https://www.instagram.com/"+instagram+"' target='_blank'>"+instagram+"</a>");
      }else{
      $(".instagram").html("Sin definir");
      }



      $("#modal_usuarios").modal("open");

    });
    </script>
