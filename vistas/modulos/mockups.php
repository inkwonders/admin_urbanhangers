<div class="encabezado barra_home">
        <div class="cont_encabezado_home">
            <div id="encabezado_vista" class="encabezado-celda">
            </div>
            <div id="encabezado_categoria">
                <div class="titulo">DISEÑOS</div>
            </div>
            <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
            </div>
        </div>
    </div>
    <div class="contenedor_interno">
        <table id="datatable_5" class="display" style="width:100%">
            <thead>
                <tr>
                    <th class="oculto">Fecha Diseño</th>
                    <th class="t_imagen_mk">Imagen</th>
                    <th class="t_diseno_mk">Diseño</th>
                    <th class="t_usuarios_mk">Usuario</th>
                    <th class="t_total_mk">Mockups</th>
                    <th class="t_acciones_mk">Acciones</th>
                </tr>
            </thead>
            <tbody>
              <?php

              $ruta_hangers_zip = Rutas::ruta_hangers_zip();

              $disenos = ControladorDisenos::ctrConsultaDisenosUsuarios();

              $cont_usuarios = 0;

              foreach($disenos as $key => $valueDisenos){
                $fecha_diseno = $valueDisenos["fecha_diseno"];
                $id_diseno = $valueDisenos["id_diseno"];
                $clave_diseno = $valueDisenos["clave_diseno"];
                $nombre_diseno = $valueDisenos["nombre_diseno"];
                $id_usuario = $valueDisenos["id_usuario"];
                $usuario = $valueDisenos["usuario"];
                $carpeta = $valueDisenos["carpeta"];
                $carpeta_mockup = $valueDisenos["carpeta_mockup"];
                $ruta_img = $valueDisenos["ruta_img"];
                $ruta_diseno = $valueDisenos["ruta"];
                $ruta_usuario = $valueDisenos["ruta_usuario"];
                $cd = $carpeta."/".$carpeta_mockup;
                $total_disenos = ControladorDisenos::ctrConsultaTotalMockupsDiseno($id_diseno);
                ?>
                <tr>
                  <td class="oculto"><?php echo $fecha_diseno; ?></td>
                  <td class="t_imagen_mk"><img class="imgdista" src="<?php echo $ruta_hangers.'vistas/assets/hangers/'.$carpeta.'/'.$ruta_img; ?>" alt="<?php echo $nombre_diseno; ?>" title="<?php echo $nombre_diseno; ?>"></td>
                  <td class="t_diseno_mk"><a class="link_disenos" href="<?php echo $ruta_hangers.$ruta_diseno; ?>" target="_blank"><?php echo $nombre_diseno; ?></a></td>
                  <td class="t_usuarios_mk"><a class="link_disenos" href="<?php echo $ruta_hangers.$ruta_usuario; ?>" target="_blank"><?php echo $usuario; ?></a></td>
                  <td class="t_total_mk"><?php echo $total_disenos['total']; ?></td>
                  <td>
                    <div class="esp_acciones_mk">
                      <a data-position="bottom" data-tooltip="Ver más" nombre="<?php echo $nombre_diseno; ?>" key="<?php echo base64_encode($id_diseno); ?>" cd="<?php echo base64_encode($cd); ?>" class="btn_info_mk tooltipped btn-floating btn-medium waves-effect waves-light grey darken-4 evento_info"><i class="material-icons">search</i></a>
                      <a data-position="bottom" data-tooltip="Descargar todos" data="<?php echo base64_encode($ruta_hangers_zip.$cd); ?>" class="tooltipped btn-floating btn-medium waves-effect waves-light blue darken-2 evento_donwload"><i class="material-icons">get_app</i></a>
                    </div>
                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody>
        </table>
    </div>

    <div id="modal_detalle" class="modal modal_dashboard_diseno">
      <span class="close_modal_detalle">X</span>
      <div id="datos_modal_dashboard" class="modal-content wh_100 fx_modal_content_dashboard" style="padding:0"></div>
    </div>

    <script type="text/javascript">

    $(".evento_info").click(function() {
      let key = $(this).attr('key');
      let nombre_diseno = $(this).attr('nombre');
      let cd = $(this).attr('cd');


      $('#modal_detalle').modal('open');

      let datos = {
        "id_diseno": key,
        "nombre_diseno" : nombre_diseno,
        "cd" : cd
      }

      $.ajax({
      	url: 'ajax/modalMockups.ajax.php',
        data: datos,
        type: "POST",
        beforeSend: function(){
          $('#datos_modal_dashboard').html(loader());
        },
      	success: function(respuesta) {
          // console.log(respuesta);
          $('#datos_modal_dashboard').html(respuesta);
          $('.tooltipped').tooltip();
      	}
      });

    });

    // $(".evento_donwload").click(function() {
    $(document).on( "click", ".evento_donwload", function() {

      var carpeta = $(this).attr("data");
      window.location.href = "ajax/deacargarzip_ajax.php?carpeta=" + carpeta;

    });

    $(".close_modal_detalle").click(function (){
      $('#modal_detalle').modal("close");
    });


    </script>
