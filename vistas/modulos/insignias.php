<div class="encabezado barra_home">
        <div class="cont_encabezado_home">
            <div id="encabezado_vista" class="encabezado-celda">
            </div>
            <div id="encabezado_categoria">
                <div class="titulo">USUARIOS</div>
            </div>
            <div id="encabezado_filtro" class="encabezado-celda evento_filtro_home" style="text-align:right">
            </div>
        </div>
    </div>
    <div class="contenedor_interno">
        <table id="datatable_4" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Medallas</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $peticion = Usuarios::consultaHangers("usuarios");

              $cont_usuarios = 0;

              foreach($peticion as $key => $hanger){
                $idUsuario = $hanger["id_usuario"];
                ?>
                <tr>

                  <!--usuario-->
                    <td><?php echo $hanger["usuario"];?></td>
                    <!--medallas-->
                    <?php
                    $medallas_total = Usuarios::consultaMedallas("insignias");
                    if($hanger["tipo_usuario"] == 1){
                      $cont_usuarios++;
                      ?>
                      <td class="espacio_medallas">
                        <?php
                        $cont_medallas = 0;
                        foreach($medallas_total as $key => $medallas){
                            $cont_medallas++;
                            $insigniasId = $medallas["id_insignia"];
                            $insigniasActivas = Usuarios::consultaInsigniasActivas('insignias_usuarios', $idUsuario, $insigniasId);

                            if($insigniasActivas[0] != 0){
                              ?>
                              <img data-position="bottom" data-tooltip="<?php echo $medallas["nombre_insignia"]; ?>" id="<?php echo 'id_'.$cont_usuarios.'_'.$cont_medallas; ?>" class="img_medalla tooltipped ev_insignias" key="<?php echo base64_encode($insigniasId); ?>" key2="<?php echo base64_encode($idUsuario); ?>" val="1" nm="<?php echo $medallas["ruta_insignia"]; ?>" src="<?php echo $ruta_hangers; ?>vistas/assets/img/<?php echo $medallas["ruta_insignia"]; ?>.svg" title="<?php echo $medallas["nombre_insignia"]; ?>">
                              <?php
                            }else{
                              ?>
                              <img data-position="bottom" data-tooltip="<?php echo $medallas["nombre_insignia"]; ?>" id="<?php echo 'id_'.$cont_usuarios.'_'.$cont_medallas; ?>" class="img_medalla tooltipped ev_insignias" key="<?php echo base64_encode($insigniasId); ?>" key2="<?php echo base64_encode($idUsuario); ?>" val="0" nm="<?php echo $medallas["ruta_insignia"]; ?>" src="<?php echo $ruta_hangers; ?>vistas/assets/img/<?php echo $medallas["ruta_insignia"]; ?>-gris.svg" title="<?php echo $medallas["nombre_insignia"]; ?>">
                              <?php
                            }
                          }
                        ?>
                      </td>
                      <?php
                    }else{
                      ?>
                      <td>No es hanger</td>
                      <?php
                    }
                    ?>

                </tr>
                <?php
              }
              ?>
            </tbody>
        </table>
    </div>



    <script type="text/javascript">

    $(".ev_insignias").click(function() {
      let id = $(this).attr('id');
      let key = $(this).attr('key');
      let key_user = $(this).attr('key2');
      let nombre_img = $(this).attr('nm');
      let status = $(this).attr('val');
      let new_val = '';
      let new_img = '';
      if(status == '1'){
        new_val = 0;
        new_img = '<?php echo $ruta_hangers; ?>vistas/assets/img/'+nombre_img+'-gris.svg';
      }else {
        new_val = 1;
        new_img = '<?php echo $ruta_hangers; ?>vistas/assets/img/'+nombre_img+'.svg';
      }

      let datos = {
        "key": key,
        "key_user" : key_user,
        "status" : status
      }

      $.ajax({
      	url: 'ajax/updateInsignias.ajax.php',
        data: datos,
        type: "POST",
      	success: function(response) {
          // console.log(response);
          if(response == 'ok'){
            $("#"+id).attr('val',new_val);
            $("#"+id).attr('src',new_img);
            M.toast({html: 'Cambio exitoso!'});
          }else {
            M.toast({html: 'Ocurrió un error!'});
          }
      	}
      });

    });

    </script>
