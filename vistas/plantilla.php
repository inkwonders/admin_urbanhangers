<?php

  session_start();

  $ruta_global = Rutas::ruta();
  $ruta_hangers = Rutas::ruta_hangers();

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>URBAN HANGERS® | Admin</title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $ruta_global; ?>vistas/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $ruta_global; ?>vistas/assets/favicon/favicon-16x16.png">
    <link rel="mask-icon" href="<?php echo $ruta_global; ?>vistas/assets/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/custom-select.css"  media="screen,projection"/>
    <link rel="stylesheet" href="<?php echo $ruta_global; ?>vistas/assets/css/estilos.css">

    <!-- WEBMANIFEST -->
    <link rel='manifest' href='manifest.webmanifest'>

    <!-- PWA -->

   <!-- <script type="module" src="pwabuilder-sw-register.js"></script>
    <script type="module" src="pwabuilder-sw.js"></script>-->

    <!-- PWA END -->

    <script src="<?php echo $ruta_global; ?>vistas/assets/js/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <!-- <script src="vistas/assets/js/spanish.json"></script> -->
    <script type="text/javascript" src="<?php echo $ruta_global; ?>vistas/assets/js/materialize.min.js"></script>
    <script src="<?php echo $ruta_global; ?>vistas/assets/js/script.js" charset="utf-8"></script>
</head>


  <?php




        if(isset($_SESSION["iniciarSesion_admin"]) && $_SESSION["iniciarSesion_admin"] == "ok"){
          if(isset($_GET["pagina"])){
            $rutas = explode("/", $_GET["pagina"]);
            if($rutas[0] === "usuarios"){

                echo "<body class='hold-transition sidebar-mini principal'>";
                include "vistas/modulos/header.php";
                include "vistas/modulos/usuarios.php";

              }else if ($rutas[0]=="disenos") {
                echo "<body class='hold-transition sidebar-mini principal'>";
                include "vistas/modulos/header.php";
                include "vistas/modulos/disenos.php";
              }else if ($rutas[0]=="mockups") {
                echo "<body class='hold-transition sidebar-mini principal'>";
                include "vistas/modulos/header.php";
                include "vistas/modulos/mockups.php";
              }else if($rutas[0]=="colecciones"){
                echo "<body class='hold-transition sidebar-mini principal'>";
                include "vistas/modulos/header.php";
                include "vistas/modulos/colecciones.php";
              }else if($rutas[0]=="insignias"){
                echo "<body class='hold-transition sidebar-mini principal'>";
                include "vistas/modulos/header.php";
                include "vistas/modulos/insignias.php";
              }else if($rutas[0] === "salir"){
                include "vistas/modulos/salir.php";
              }else{
                include "vistas/modulos/404.php";
              }
            }else{
              echo "<body class='hold-transition sidebar-mini principal'>";
              include "vistas/modulos/header.php";
              include "vistas/modulos/usuarios.php";
            }
          }else{
            if(isset($_GET["pagina"])){
              $rutas = explode("/", $_GET["pagina"]);
              if($rutas[0] === "login"){
                echo "<body class='hold-transition sidebar-mini login-page principal'>";
                include "vistas/modulos/login.php";
              }else {
                ?>
                <script type="text/javascript">
                  location.href = "login";
                </script>
                <?php
              }
            }else {
              echo "<body class='hold-transition sidebar-mini login-page principal'>";
              include "vistas/modulos/login.php";
            }
          }

  ?>

</body>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    $.extend( true, $.fn.dataTable.defaults, {
        "processing": true,
        "language": {
            "decimal": ",",
            "thousands": ".",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoPostFix": "",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "loadingRecords": "Cargando...",
            'processing': "Cargando datos ...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },

            "search": "Buscar:",
            "searchPlaceholder": "",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "aria": {
                "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            //only works for built-in buttons, not for custom buttons
            "buttons": {
                "create": "Nuevo",
                "edit": "Cambiar",
                "remove": "Borrar",
                "copy": "Copiar",
                "csv": "fichero CSV",
                "excel": "tabla Excel",
                "pdf": "documento PDF",
                "print": "Imprimir",
                "colvis": "Visibilidad columnas",
                "collection": "Colección",
                "upload": "Seleccione fichero...."
            },
            "select": {
                "rows": {
                    _: '%d filas seleccionadas',
                    0: 'clic fila para seleccionar',
                    1: 'una fila seleccionada'
                }
            }
        }
    } );
    $(document).ready(function() {
      $('.modal').modal();
      $('.tooltipped').tooltip();

        url = window.location.pathname;
        // console.log(url);
        pestaña_actual=url.split('/');
        // console.log(pestaña_actual[3]);
        if(pestaña_actual[1]=="usuarios"){
          $("#bUsuarios").css("color", "black");
          $('#datatable_1').DataTable();
        }
        if(pestaña_actual[1]=="disenos"){
          $("#bDisenos").css("color", "black");
          $('#datatable_2').DataTable({
            "order": [[ 0, "desc" ]]
          });
        }
        if(pestaña_actual[1]=="colecciones"){
          $("#bColecciones").css("color", "black");
          $('#datatable_3').DataTable();
        }
        if(pestaña_actual[1]=="insignias"){
          $("#bInsignias").css("color", "black");
          $('#datatable_4').DataTable();
        }
        if(pestaña_actual[1]=="mockups"){
          $("#bMockups").css("color", "black");
          $('#datatable_5').DataTable({
            "order": [[ 0, "desc" ]]
          });
        }
        $("select").formSelect();
        // $.extend( true, $.fn.dataTable.Editor.defaults, {
        // 	"sProcessing":     "Procesando...",
        // 	"sLengthMenu":     "Mostrar _MENU_ registros",
        // 	"sZeroRecords":    "No se encontraron resultados",
        // 	"sEmptyTable":     "Ningún dato disponible en esta tabla",
        // 	"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        // 	"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        // 	"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        // 	"sInfoPostFix":    "",
        // 	"sSearch":         "Buscar:",
        // 	"sUrl":            "",
        // 	"sInfoThousands":  ",",
        // 	"sLoadingRecords": "Cargando...",
        // 	"oPaginate": {
        // 		"sFirst":    "Primero",
        // 		"sLast":     "Último",
        // 		"sNext":     "Siguiente",
        // 		"sPrevious": "Anterior"
        // 	},
        // 	"oAria": {
        // 		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        // 		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
        // 	}
        // });
    } );

    // LOADER

    const loader = () => {
      return `<div class="progress abs_center loader_colores">
                <div class="indeterminate loader_colores2"></div>
              </div>`;
    }

    // FIN LOADER
</script>
</html>
