<?php
date_default_timezone_set('America/Mexico_City');
require_once "../controladores/colecciones.controlador.php";
require_once "../modelos/colecciones.modelo.php";

class updateColecciones{

    public function updateColeccionActiva(){
      $id_coleccion = $this->key_coleccion;
      $status = $this->status;

      if($status == 1){
        $activo = 0;
      }else {
        $activo = 1;
      }

      $query = "UPDATE colecciones SET activo = $activo WHERE sk_coleccion = '$id_coleccion'";

      $updateActivo = Colecciones::ctrUpdateColeccion($query);
      echo $updateActivo;
    }

    public function updateAgregarColeccion(){
      $nombre_coleccion = $this->nueva_coleccion;
      $genera_uuid = Colecciones::generaUUID();
      $id_coleccion = $genera_uuid['uuid'];
      $fecha_actual = date("Y-m-d H:i:s");
      $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
      $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
      $ruta = utf8_decode($nombre_coleccion);
      $ruta = strtr($ruta, utf8_decode($originales), $modificadas);
      $ruta = strtolower($ruta);
      $ruta = str_replace(" ","-",$ruta);

      $query = "INSERT INTO colecciones (sk_coleccion, nombre_coleccion, activo, fecha_alta, ruta) VALUES ('$id_coleccion', '$nombre_coleccion', '1', '$fecha_actual', '$ruta')";

      $updateActivo = Colecciones::ctrUpdateColeccion($query);
      echo $updateActivo;
    }

  }

$update = New updateColecciones();
if(!empty($_POST['tipo'])){

    if($_POST['tipo'] == 'activo'){
      $update -> key_coleccion = base64_decode($_POST['key_coleccion']);
      $update -> status = $_POST['status'];
      $update -> updateColeccionActiva();

    }else if($_POST['tipo'] == 'agregar_coleccion'){
      $update -> nueva_coleccion = $_POST['value'];
      $update -> updateAgregarColeccion();
    }else {
      echo "error";
    }
  }else {
    echo "error";
  }
