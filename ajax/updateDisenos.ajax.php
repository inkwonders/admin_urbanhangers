<?php
date_default_timezone_set('America/Mexico_City');
require_once "../controladores/disenos.controlador.php";
require_once "../modelos/disenos.modelo.php";

class updateDisenos{

    public function updateDisenoTendencia(){
      $id_usuario = $this->key_user;
      $id_diseno = $this->key;
      $clave_diseno = $this->no;
      $status = $this->status;
      $fecha_hoy = date('Y-m-d h:i:s');

      if($status == 0){
        $query = "UPDATE diseno SET tendencia = 1, fecha_tendencia = '$fecha_hoy' WHERE id_diseno = '$id_diseno' AND clave_diseno = '$clave_diseno' AND id_usuario = '$id_usuario'";
      }else {
        $query = "UPDATE diseno SET tendencia = 0 WHERE id_diseno = '$id_diseno' AND clave_diseno = '$clave_diseno' AND id_usuario = '$id_usuario'";
      }

      $updateTendencia = ControladorDisenos::ctrUpdateDiseno($query);
      echo $updateTendencia;

    }

    public function updateDisenoTienda(){
      $id_usuario = $this->key_user;
      $id_diseno = $this->key;
      $clave_diseno = $this->no;
      $ruta_tienda = $this->ruta_tienda;
      $fecha_hoy = date('Y-m-d h:i:s');

      $query = "UPDATE diseno SET diseno_en_venta = 1, ruta_tienda = '$ruta_tienda', fecha_tienda = '$fecha_hoy' WHERE id_diseno = '$id_diseno' AND clave_diseno = '$clave_diseno' AND id_usuario = '$id_usuario'";

      $updateTienda = ControladorDisenos::ctrUpdateDiseno($query);
      echo $updateTienda;

    }

    public function updateDisenoRutaTienda(){
      $id_usuario = $this->key_user;
      $id_diseno = $this->key;
      $clave_diseno = $this->no;
      $ruta_tienda = $this->ruta_tienda;

      $query = "UPDATE diseno SET diseno_en_venta = 1, ruta_tienda = '$ruta_tienda' WHERE id_diseno = '$id_diseno' AND clave_diseno = '$clave_diseno' AND id_usuario = '$id_usuario'";

      $updateTienda = ControladorDisenos::ctrUpdateDiseno($query);
      echo $updateTienda;

    }

    public function updateDisenoActivo(){
      $id_usuario = $this->key_user;
      $id_diseno = $this->key;
      $clave_diseno = $this->no;
      $status = $this->status;

      if($status == 1){
        $activo = 0;
      }else {
        $activo = 1;
      }

      $query = "UPDATE diseno SET activo = $activo WHERE id_diseno = '$id_diseno' AND clave_diseno = '$clave_diseno' AND id_usuario = '$id_usuario'";

      $updateActivo = ControladorDisenos::ctrUpdateDiseno($query);
      echo $updateActivo;
    }

  }

$update = New updateDisenos();

if(!empty($_POST['key']) && !empty($_POST['no']) && !empty($_POST['key_user']) && !empty($_POST['tipo'])){

  $update -> key_user = base64_decode($_POST['key_user']);
  $update -> key = base64_decode($_POST['key']);
  $update -> no = $_POST['no'];

  if($_POST['tipo'] == 'tendencia'){

    $update -> status = $_POST['status'];
    $update -> updateDisenoTendencia();

  }else if($_POST['tipo'] == 'tienda'){

    if(!empty($_POST['ruta_tienda'])){

      $update -> ruta_tienda = $_POST['ruta_tienda'];
      $update -> updateDisenoTienda();

    }else {

      echo "error";

    }

  }else if($_POST['tipo'] == 'activo'){

    $update -> status = $_POST['status'];
    $update -> updateDisenoActivo();

  }else if($_POST['tipo'] == 'ruta_tienda'){
    $update -> ruta_tienda = $_POST['ruta_tienda'];
    $update -> updateDisenoRutaTienda();
  }

}else {
  echo "error";
}
