<?php
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/dashboard.controlador.php";
require_once "../controladores/disenos.controlador.php";
require_once "../controladores/usuarios.controlador.php";

require_once "../modelos/dashboard.modelo.php";
require_once "../modelos/disenos.modelo.php";
require_once "../modelos/usuarios.modelo.php";

  class modalDiseno{

    public function datos_modal(){

      $ruta_hangers = Rutas::ruta_hangers();
      $id_diseno = base64_decode($this->id_diseno);
      $id_usuario = base64_decode($this->id_usuario);
      $nombre = $this->nombre;
      $inspiracion = $this->inspiracion;
      $carpeta = base64_decode($this->carpeta);

        $ruta = ControladorDashboard::consultaRuta('diseno', $id_diseno);
        $rutaDiseno = $ruta["ruta"];

        $datosUsuario = Usuarios::ctrDatosUsuario('usuarios',$id_usuario);
        ?>
        <div id="contenedor-slider" class="cont_img_dashboard_modal">
          <?php
          $respuesta = ControladorDisenos::consultaImgDiseno('diseno', $id_diseno);
          $ruta_img = $respuesta['ruta_img'];
          ?>
            <img id='img_slider_hanger' class='brd_rd' src='<?php echo $ruta_hangers; ?>vistas/assets/hangers/<?php echo $carpeta."/".$ruta_img; ?>'>
        </div>
        <div id="contenedor-datos">
          <div class="cont_usuario_modal">
            <span class="span_usuario_modal">
              Usuario:
              <a class="usuario_modal_link" href="<?php echo $ruta_hangers.$datosUsuario['ruta']; ?>"><?php echo $datosUsuario['usuario']; ?></a>
            </span>
          </div>
          <div class="cont_nombre_votos">
            <div class="cont_nombre">
              <h3 class="nombre-prenda nom_modal_dsh" id="nombre_venta">"<?php echo $nombre; ?>"</h3>
            </div>
            <div class="cont_vot">
              <div id="conteo_votos<?php echo $id_diseno; ?>" class="cont_votos_corazon_dash">
              <?php
                $votos = ControladorDisenos::consultaVotos('votos_diseno', $id_diseno);
              ?>
              <div id='<?php echo $id_diseno; ?>' class='votado votado_fx'></div>
              <p id="numero_votos<?php echo $id_diseno; ?>" numero_de_votos="<?php echo $votos[0]?>" class="conteo conteo_fx"><?php echo $votos[0] > 0 ?  $votos[0]."<br /> VOTOS": "SIN <br /> VOTOS" ?></p>
            </div>
            </div>
          </div>
          <div id="contenedor-inspiracion">
            <ul class="collapsible">
              <li>
                <div id="linea_inspiracion" class="collapsible-header" style="cursor:auto;"></div>
                <div id="inspiracion_modal_dashboard" class="collapsible-body" style="display: flex;">
                  <span id="inspo_venta" class='wp'><?php echo $inspiracion; ?></span>
                </div>
              </li>
            </ul>
          </div>
          <div class="cont_vot_com_dash">
            <?php
              $ruta_wa = "https://web.whatsapp.com/send?text=https://clientes.ink&#47urbanhangers/".$rutaDiseno;
              $ruta_tw = "https://twitter.com/intent/tweet?text=https://clientes.ink&#47urbanhangers/".$rutaDiseno;
              $ruta_fb = "http://www.facebook.com/sharer.php?u=https://clientes.ink&#47urbanhangers&#47".$rutaDiseno;
            ?>
            <div id="comentarios_modal_dashboard" class="comentarios">
              <div class="compartelo_dash pad_btm"><p id="compartelo_modal_dashboard"><b>COMPÁRTELO</b></p></div>
              <div class="redes pad_btm">
                <a id="wa_<?php echo $id_diseno; ?>" class="ev_meta" dis="<?php echo $id_diseno; ?>" href="<?php echo $ruta_wa; ?>" data-action="share/whatsapp/share" target="_blank"><img class="icon-red-modal" src="<?php echo $ruta_hangers?>vistas/assets/img/icon-whats-o.svg" /></a>
                <a id="tw_<?php echo $id_diseno; ?>" class="ev_meta" dis="<?php echo $id_diseno; ?>" href="<?php echo $ruta_tw; ?>" target="_blank"><img class="icon-red-modal" src="<?php echo $ruta_hangers?>vistas/assets/img/icon-twitter-o.svg" /></a>
                <a id="fb_<?php echo $id_diseno; ?>" class="ev_meta" dis="<?php echo $id_diseno; ?>" href="<?php echo $ruta_fb; ?>" target="_blank"><img class="icon-red-modal" src="<?php echo $ruta_hangers?>vistas/assets/img/icon-face-o.svg"  /></a>
              </div>

              <div id="comentarios<?php echo $id_diseno; ?>" class="comen_scroll_dash">

                 <?php

                    $comentarios = ControladorDisenos::consultaComentarios('usuarios', $id_diseno);

                    if(empty($comentarios)){
                      echo "<div class='sin_comentarios'><span class='comentario-nombre'>Sin comentarios</span></div>";
                    }else{

                      foreach($comentarios as $key => $valueComentarios){

                ?>

                <div class='agregar_comentario'>

                  <div class='comentario-perfil'>
                    <?php
                    if($valueComentarios['tipo_usuario'] == 2){
                      if($valueComentarios['sexo'] == "h"){
                        echo '<img class="comentario-img" src="'.$ruta_hangers.'vistas/assets/img/icon-usuario-1.svg">';
                      }else {
                        echo '<img class="comentario-img" src="'.$ruta_hangers.'vistas/assets/img/icon-usuario-5.svg">';
                      }
                    }else if($valueComentarios['tipo_usuario'] == 1){
                      if($valueComentarios['modo_registro'] == "facebook"){
                        echo '<img class="comentario-img" src="'.$valueComentarios['foto'].'">';
                      }else {
                        if(!empty($valueComentarios['foto'])){
                          echo '<img class="comentario-img" src="'.$ruta_hangers.'vistas/assets/hangers/'.$valueComentarios['carpeta'].'/'.$valueComentarios['foto'].'">';
                        }else {
                          if($valueComentarios['sexo'] == "h"){
                            echo '<img class="comentario-img" src="'.$ruta_hangers.'vistas/assets/img/icon-usuario-1.svg">';
                          }else {
                            echo '<img class="comentario-img" src="'.$ruta_hangers.'vistas/assets/img/icon-usuario-5.svg">';
                          }
                        }
                        // echo '<img class="comentario-img" src="vistas/assets/img/'.$valueComentarios['foto'].'">';
                      }
                    }
                    ?>
                  </div>

                  <div class='comentario_descripcion fx_comentarios'>
                    <span class='comentario-nombre negritas'>
                      <?php
                        echo $valueComentarios['usuario'];
                      ?>
                    </span>
                    <span class='comentario-texto'><?php echo $valueComentarios['comentario']; ?></span>
                    <?php
                    $mes = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sept","Oct","Nov","Dic");
                    $mes_comentario = $mes[date('n', strtotime($valueComentarios['fecha']))-1];
                    $dia = date("d", strtotime($valueComentarios['fecha']));
                    $año = date("Y", strtotime($valueComentarios['fecha']));
                    $hora = date("g:i A",strtotime($valueComentarios['fecha']));
                    $fecha_hora =  $dia."-".$mes_comentario."-".$año." ".$hora;
                    ?>
                    <span class='comentario-tiempo'><?php echo $fecha_hora;?></span>
                  </div>
                </div>
              <?php
                      }
                    }
              ?>
              </div>
            </div>
          </div>
        </div>

      <?php

    }

  }


  $datos = new modalDiseno();

  if( !empty($_POST['id_diseno']) && !empty($_POST['id_usuario']) && !empty($_POST['nombre_diseno']) && !empty($_POST['inspiracion_diseno']) && !empty($_POST['carpeta']) ){

    $datos -> id_diseno = $_POST["id_diseno"];
    $datos -> id_usuario = $_POST["id_usuario"];
    $datos -> nombre = $_POST["nombre_diseno"];
    $datos -> inspiracion = $_POST["inspiracion_diseno"];
    $datos -> carpeta = $_POST["carpeta"];

    $datos -> datos_modal();

  }else{
    echo "error";
  }
?>
