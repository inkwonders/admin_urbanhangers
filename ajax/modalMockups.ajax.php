<?php
require_once "../controladores/rutas.controlador.php";
require_once "../controladores/disenos.controlador.php";

require_once "../modelos/disenos.modelo.php";

class modalMockups
{

  public function datos_modal()
  {

    $ruta_hangers = Rutas::ruta_hangers();
    $ruta_hangers_zip = Rutas::ruta_hangers_zip();
    $ruta_download = "../../../urbanhangerscommunity.com/vistas/assets/hangers/";
    $id_diseno = base64_decode($this->id_diseno);
    $nombre_diseno = $this->nombre_diseno;
    $cd = base64_decode($this->cd);
?>
    <div class="cont_modal2">
      <div class="esp_nombre_boton">
        <p class="nombre_diseno"><?php echo $nombre_diseno; ?></p>
        <a data-position="bottom" data-tooltip="Descargar todos" data="<?php echo base64_encode($ruta_hangers_zip . $cd); ?>" class="btn_descargar_modal tooltipped btn-floating btn-medium waves-effect waves-light blue darken-2 evento_donwload"><i class="material-icons">get_app</i></a>
      </div>
      <div class="esp_mockups">
        <?php
        $mockups = ControladorDisenos::ctrConsultaMockupsDiseno($id_diseno);
        foreach ($mockups as $key => $valueMockups) {
          $producto = $valueMockups['producto'];
          if ($valueMockups['producto'] == 1) {
            $genero = "Hombre";
          } else if ($valueMockups['producto'] == 2) {
            $genero = "Mujer";
          } else if ($valueMockups['producto'] == 3) {
            $genero = "Unisex";
          }
          $nombre_archivo = $valueMockups['nombre_archivo'];
          $nombre_original = $valueMockups['nombre_original'];
        ?>
          <div class="cont_img_modal">          
            <a target="_blank" href="<?php echo $ruta_hangers; ?>vistas/assets/hangers/<?php echo $cd . $nombre_archivo; ?>">
              <img data-position="bottom" data-tooltip="<?php echo $nombre_original; ?>" class="tooltipped img_mockup" src="<?php echo $ruta_hangers; ?>vistas/assets/hangers/<?php echo $cd . $nombre_archivo; ?>">
            </a> 
          </div>
        <?php
        }
        ?>
      </div>
    </div>
<?php
  }
}


$datos = new modalMockups();

if (!empty($_POST['id_diseno']) && !empty($_POST['nombre_diseno']) && !empty($_POST['cd'])) {

  $datos->id_diseno = $_POST["id_diseno"];
  $datos->nombre_diseno = $_POST["nombre_diseno"];
  $datos->cd = $_POST["cd"];

  $datos->datos_modal();
} else {
  echo "error";
}
?>