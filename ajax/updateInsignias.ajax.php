<?php

date_default_timezone_set('America/Mexico_City');
require_once "../controladores/insignias.controlador.php";
require_once "../modelos/insignias.modelo.php";

class updateInsignias{

    public function actualizaInsignias(){

      $id_insignia = $this->key;
      $id_usuario = $this->key_user;
      $status = $this->status;

      if($status == "1"){        

        $respuesta = ControladorInsignias::ctrEliminarInsignia('insignias_usuarios',$id_usuario,$id_insignia);

      }else {

        $genera_uuid = ControladorInsignias::generaUUID();
        $respuesta = ControladorInsignias::ctrAgregarInsignia('insignias_usuarios',$genera_uuid['uuid'],$id_usuario,$id_insignia);

      }

      echo $respuesta;

    }

  }

$update = New updateInsignias();

if(!empty($_POST['key']) && !empty($_POST['key_user'])){

  $update -> key_user = base64_decode($_POST['key_user']);
  $update -> key = base64_decode($_POST['key']);
  $update -> status = $_POST['status'];

  $update -> actualizaInsignias();

}
