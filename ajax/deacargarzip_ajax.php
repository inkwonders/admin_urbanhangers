<?php

  $carpeta = base64_decode($_GET["carpeta"]);
  // Creamos un instancia de la clase ZipArchive
  $zip = new ZipArchive();

    // echo " carpeta = ".$carpeta." | ";

  // Creamos y abrimos un archivo zip temporal
  $zip->open($GLOBALS["carpeta"]."mockups.zip",ZipArchive::CREATE);
  // Añadimos un directorio
  $dir = 'mockups';
  $zip->addEmptyDir($dir);

  showFiles($carpeta);

  $zip->close();



// Creamos las cabezeras que forzaran la descarga del archivo como archivo zip.
header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=mockups.zip");


// leemos el archivo creado
readfile($GLOBALS["carpeta"].'mockups.zip');

// Por último eliminamos el archivo temporal creado
unlink($GLOBALS["carpeta"].'mockups.zip');//Destruye el archivo temporal


function showFiles($path){

    // echo " entro show ".$path." | ";

    $leer_dir = opendir($path);

    while($current = readdir($leer_dir)){

      // echo " current = ".$current." | ";

      if($current != "." && $current != ".."){

        if($current != "mockups.zip"){

          $ruta_archivo = $GLOBALS['carpeta'].$current;
          $GLOBALS['zip']->addFile($ruta_archivo, "mockups/".$current);

        }

      }

    }
    
}
