<?php

class ControladorDisenos{

  static public function consultaDisenos($tabla1){
     $respuesta = ModeloDisenos::consultaDisenos($tabla1);
     return $respuesta;
  }

  static public function consultaVotos($tabla1, $idDiseno){
    $respuesta = ModeloDisenos::consultaVotos($tabla1, $idDiseno);
    return $respuesta;
  }

  static public function consultaComentarios($tabla1, $idDiseno){
    $respuesta = ModeloDisenos::consultaComentarios($tabla1, $idDiseno);
    return $respuesta;
  }

  static public function consultaImgDiseno($tabla1, $key){
    $respuesta = ModeloDisenos::consultaImgDiseno($tabla1, $key);
    return $respuesta;
  }

  static public function ctrUpdateDiseno($query){
    $respuesta = ModeloDisenos::mdlUpdateDiseno($query);
    return $respuesta;
  }

  static public function ctrConsultaDisenosUsuarios(){
    $respuesta = ModeloDisenos::mdlConsultaDisenosUsuarios();
    return $respuesta;
  }

  static public function ctrConsultaMockupsDiseno($id_diseno){
    $respuesta = ModeloDisenos::mdlConsultaMockupsDiseno($id_diseno);
    return $respuesta;
  }

  static public function ctrConsultaTotalMockupsDiseno($id_diseno){
    $respuesta = ModeloDisenos::mdlConsultaTotalMockupsDiseno($id_diseno);
    return $respuesta;
  }
}
