<?php

  class ControladorInsignias{

    static public function ctrAgregarInsignia($tabla,$id_insignia_usuario,$id_usuario,$id_insignia)
    {

      $respuesta = ModeloInsignias::mdlAgregarInsignia($tabla,$id_insignia_usuario,$id_usuario,$id_insignia);
      return $respuesta;

  	}

    static public function ctrEliminarInsignia($tabla,$id_usuario,$id_insignia)
    {

      $respuesta = ModeloInsignias::mdlEliminarInsignia($tabla,$id_usuario,$id_insignia);
      return $respuesta;

    }

    static public function generaUUID(){

      $uuid = ModeloInsignias::generaUUID();

      return $uuid;

    }

  }
