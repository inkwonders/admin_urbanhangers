<?php

class ControladorDashboard{

  static public function consultaId($tabla, $ruta){

    $respuesta = dashboardModelo::consultaId($tabla, $ruta);

    return $respuesta;

  }

  static public function consultaDashboard($tabla, $id){

     $respuesta = dashboardModelo::consultaDashboard($tabla, $id);

     return $respuesta;

  }

  static public function consultaInsignias($tabla1){

    $respuesta = dashboardModelo::consultaInsignias($tabla1);

    return $respuesta;

  }

  static public function consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId){

    $respuesta = dashboardModelo::consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId);

    return $respuesta;

  }

  static public function consultaNoDisenos($tabla1, $tabla2, $idUsuario){

    $respuesta = dashboardModelo::consultaNoDisenos($tabla1, $tabla2, $idUsuario);

    return $respuesta;

  }

  static public function consultaNoLikes($tabla1, $tabla2, $idUsuario){

    $respuesta = dashboardModelo::consultaNoLikes($tabla1, $tabla2, $idUsuario);

    return $respuesta;
  }

  static public function consultaRedes($tabla1, $idUsuario){

    $respuesta = dashboardModelo::consultaRedes($tabla1, $idUsuario);

    return $respuesta;

  }

  static public function consultaSlideFiltro($idUsuario, $filtro){

    $respuesta = dashboardModelo::consultaSlideFiltro($idUsuario, $filtro);

    return $respuesta;

  }

  static public function consultaSlideFiltroTienda($idUsuario, $filtro){

    $respuesta = dashboardModelo::consultaSlideFiltroTienda($idUsuario, $filtro);

    return $respuesta;

  }

  static public function consultaImgSlideNuevos($tabla1, $idDisenoNuevo){

    $respuesta = dashboardModelo::consultaImgSlideNuevos($tabla1, $idDisenoNuevo);

    return $respuesta;

  }

  static public function consultaVotos($tabla1, $idDisenoNuevo){

    $respuesta = dashboardModelo::consultaVotos($tabla1, $idDisenoNuevo);

    return $respuesta;

  }

  static public function consultaVentas($tabla1, $idDisenoNuevo){

    $respuesta = dashboardModelo::consultaVentas($tabla1, $idDisenoNuevo);

    return $respuesta;

  }

  static public function consultaSlideVenta($tabla1, $idUsuario){

    $respuesta = dashboardModelo::consultaSlideVenta($tabla1, $idUsuario);

    return $respuesta;

  }

  static public function consultaImgSlideVenta($tabla1, $idDisenoVenta){

    $respuesta = dashboardModelo::consultaImgSlideVenta($tabla1, $idDisenoVenta);

    return $respuesta;

  }

  static public function consultaVotosVenta($tabla1, $idDisenoVenta){

    $respuesta = dashboardModelo::consultaVotos($tabla1, $idDisenoVenta);

    return $respuesta;

  }

  static public function consultaVentasVenta($tabla1, $idDisenoVenta){

    $respuesta = dashboardModelo::consultaVentasVenta($tabla1, $idDisenoVenta);

    return $respuesta;

  }

  static public function consultaEditarPerfil($tabla, $id){

     $respuesta = dashboardModelo::consultaEditarPerfil($tabla, $id);

     return $respuesta;

  }

  static public function consultaRuta($tabla, $id){

    $respuesta = dashboardModelo::consultaRuta($tabla, $id);

    return $respuesta;

 }


}


?>
