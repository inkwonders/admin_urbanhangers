<?php

  class Usuarios{

    /*=============================================
  	INGRESO DE USUARIO
  	=============================================*/

  	static public function ctrIngreso(){

  		if(isset($_POST["email_login"])){

  			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["email_login"])){

  			  $encriptar = crypt($_POST["contraseña_login"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

  				$tabla = "usuarios";

          $correo = $_POST["email_login"];

  				$respuesta = ModeloUsuarios::mdlConsultaUsuario($tabla, $correo);

  				if($respuesta["correo"] == $_POST["email_login"] && $respuesta["password"] == $encriptar && $respuesta["tipo_usuario"] == 3){

            if (headers_sent()) {
            }else {
            session_start();
          }

						$_SESSION["iniciarSesion_admin"] = "ok";
						$_SESSION["tipoUsuario_admin"] = $respuesta["tipo_usuario"];
            $_SESSION["id_usuario_admin"] = $respuesta["id_usuario"];
            $_SESSION["usuario_admin"] = $respuesta["usuario"];
						$_SESSION["nombre_admin"] = $respuesta["nombre"];
            $_SESSION["apellido_admin"] = $respuesta["apellido"];
            $_SESSION["correo_admin"] = $respuesta["correo"];

						echo "<script> location.href='usuarios'; </script>";

  				}else{

            echo "Usuario o contraseña incorrectos...";

  				}

  			}

  		}

  	}

    static public function consultaUsuarios($tabla){
      $respuesta = ModeloUsuarios::consultaUsuarios($tabla);

      return $respuesta;

    }
    static public function consultaHangers($tabla){
      $respuesta = ModeloUsuarios::consultaHangers($tabla);

      return $respuesta;

    }
    static public function consultaNumeroDisenos($tabla, $idUsuario){
      $respuesta = ModeloUsuarios::consultaNumeroDisenos($tabla, $idUsuario);

      return $respuesta;

    }
    static public function consultaNumeroVotos($tabla, $idUsuario){
      $respuesta = ModeloUsuarios::consultaNumeroVotos($tabla, $idUsuario);

      return $respuesta;

    }
    static public function consultaMedallas($tabla){
      $respuesta = ModeloUsuarios::consultaMedallas($tabla);

      return $respuesta;

    }
    static public function consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId){
      $respuesta = ModeloUsuarios::consultaInsigniasActivas($tabla1, $idUsuario, $insigniasId);

      return $respuesta;

    }

    static public function consultaDisenosTienda($tabla1, $idUsuario){
      $respuesta = ModeloUsuarios::consultaDisenosTienda($tabla1, $idUsuario);

      return $respuesta;

    }

    static public function consultaDisenoMasVotos($tabla1, $tabla2, $idUsuario){
      $respuesta = ModeloUsuarios::consultaDisenoMasVotos($tabla1, $tabla2, $idUsuario);

      return $respuesta;

    }

    static public function consultaPaisUsuario($tabla, $idPais){
      $respuesta = ModeloUsuarios::consultaPaisUsuario($tabla, $idPais);

      return $respuesta;

    }

    static public function consultaEstadoUsuario($tabla, $idPais){
      $respuesta = ModeloUsuarios::consultaEstadoUsuario($tabla, $idPais);

      return $respuesta;

    }
    static public function ctrUpdateUsuario($query){
      $respuesta = ModeloUsuarios::mdlUpdateUsuario($query);
      return $respuesta;
    }

    static public function ctrDatosUsuario($tabla,$id_usuario){
      $respuesta = ModeloUsuarios::mdlDatosUsuario($tabla,$id_usuario);
      return $respuesta;
    }

  }
