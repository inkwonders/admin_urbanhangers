<?php

  class Colecciones{

    static public function consultaColecciones($tabla){
      $respuesta = ModeloColecciones::consultaColecciones($tabla);

      return $respuesta;
  	}

    static public function ctrUpdateColeccion($query){
      $respuesta = ModeloColecciones::mdlUpdateColeccion($query);
      return $respuesta;
    }

    static public function generaUUID(){

      $uuid = ModeloColecciones::generaUUID();

      return $uuid;

    }
    




  }
