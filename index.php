<?php

	// error_reporting(E_ALL);
	// ini_set('display_errors', '1');

	require_once "controladores/plantilla.controlador.php";
	require_once "controladores/rutas.controlador.php";
	require_once "controladores/usuarios.controlador.php";
	require_once "controladores/disenos.controlador.php";
	require_once "controladores/colecciones.controlador.php";
	require_once "controladores/insignias.controlador.php";

	require_once "modelos/usuarios.modelo.php";
	require_once "modelos/disenos.modelo.php";
	require_once "modelos/colecciones.modelo.php";
	require_once "modelos/insignias.modelo.php";

	$plantilla = new ControladorPlantilla();
	$plantilla -> plantilla();
